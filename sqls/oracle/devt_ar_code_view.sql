CREATE OR replace VIEW devt_ar_code_view 
AS 
  SELECT name, 
         seq, 
         childname, 
         SUBSTR('000000000000000' 
               + REPLACE(CAST(schemaid AS varchar2(30)) 
			   + CAST(objectid AS varchar2(30)) 
			   + CAST(objecttype AS varchar2(30)) 
			   + CAST(childid AS varchar2(30)), 
               ' ', ''), -15) request_id, 
         schemaid, 
         objectid, 
         objecttype 
  FROM   (SELECT al.name, 
                 0            seq, 
                 NULL         childName, 
                 ' '          childId, 
                 am.schemaid, 
                 al.actlinkid ObjectId, 
                 0            ObjectType 
          FROM   actlink al 
                 inner join actlink_mapping am 
                         ON al.actlinkid = am.actlinkid 
          UNION ALL 
          SELECT f.name, 
                 0          seq, 
                 NULL       childName, 
                 ' '        childId, 
                 fm.schemaid, 
                 f.filterid ObjectId, 
                 1          ObjectType 
          FROM   filter f 
                 inner join filter_mapping fm 
                         ON f.filterid = fm.filterid 
          UNION ALL 
          SELECT e.name, 
                 0              seq, 
                 NULL           childName, 
                 ' '            childId, 
                 em.schemaid, 
                 e.escalationid ObjectId, 
                 2              ObjectType 
          FROM   escalation e 
                 inner join escal_mapping em 
                         ON e.escalationid = em.escalationid 
          UNION ALL 
          SELECT arc.name, 
                 arr.referenceorder seq, 
                 al.name            childName, 
                 to_char(al.actlinkid)       childId, 
                 coo.ownerobjid     schemaId, 
                 arc.containerid    ObjectId, 
                 3                  ObjectType 
          FROM   arcontainer arc 
                 inner join cntnr_ownr_obj coo 
                         ON arc.containerid = coo.containerid 
                 inner join arreference arr 
                         ON arc.containerid = arr.containerid 
                 inner join actlink al 
                         ON arr.referenceobjid = al.actlinkid 
          WHERE  ( arc.containertype = 1 ) 
          UNION ALL 
          SELECT arc.name, 
                 arr.referenceorder seq, 
                 f.name             childName, 
                 to_char(f.filterid)         childId, 
                 coo.ownerobjid     schemaId, 
                 arc.containerid    ObjectId, 
                 4                  ObjectType 
          FROM   arcontainer arc 
                 inner join cntnr_ownr_obj coo 
                         ON arc.containerid = coo.containerid 
                 inner join arreference arr 
                         ON arc.containerid = arr.containerid 
                 inner join filter f 
                         ON arr.referenceobjid = f.filterid 
          WHERE  ( arc.containertype = 4 )) 