CREATE TABLE COLUMN_NEXT_ID 
  ( 
     Company                NVARCHAR2(255), 
     Form_Name              NVARCHAR2(255), 
     Next_ID                NVARCHAR2(255), 
     Padding_Char           NVARCHAR2(255), 
     Hierarchy_Key_Prefix   NVARCHAR2(255), 
     Padding_Int            INT, 
     Prefix_Char            NVARCHAR2(255), 
     Prefix_Type            NVARCHAR2(255), 
     Record_Key_Field_ID    INT,
     Prefix_Separator       NVARCHAR2(255)
  ) 
