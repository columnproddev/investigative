USE @DBNAME@
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'devt_ar_code_view'))
BEGIN
drop view devt_ar_code_view
END;

CREATE VIEW devt_ar_code_view AS SELECT     name, seq, childName, RIGHT('000000000000000' + REPLACE(STR(schemaId) + STR(ObjectId) + STR(ObjectType) + STR(childId), ' ', ''), 15) 
                      AS request_id, schemaId, ObjectId, ObjectType
        FROM         (SELECT     al.name, 0 AS seq, NULL AS childName, ' ' AS childId, am.schemaId, al.actlinkId AS ObjectId, 0 AS ObjectType
                       FROM          dbo.actlink AS al INNER JOIN
                                              dbo.actlink_mapping AS am ON al.actlinkId = am.actlinkId
                       UNION ALL
                       SELECT     f.name, 0 AS seq, NULL AS childName, ' ' AS childId, fm.schemaId, f.filterId AS ObjectId, 1 AS ObjectType
                       FROM         dbo.filter AS f INNER JOIN
                                             dbo.filter_mapping AS fm ON f.filterId = fm.filterId
                       UNION ALL
                       SELECT     e.name, 0 AS seq, NULL AS childName, ' ' AS childId, em.schemaId, e.escalationId AS ObjectId, 2 AS ObjectType
                       FROM         dbo.escalation AS e INNER JOIN
                                             dbo.escal_mapping AS em ON e.escalationId = em.escalationId
                       UNION ALL
                       SELECT     arc.name, arr.referenceOrder AS seq, al.name AS childName, al.actlinkId AS childId, coo.ownerObjId AS schemaId, 
                                             arc.containerId AS ObjectId, 3 AS ObjectType
                       FROM         dbo.arcontainer AS arc INNER JOIN
                                             dbo.cntnr_ownr_obj AS coo ON arc.containerId = coo.containerId INNER JOIN
                                             dbo.arreference AS arr ON arc.containerId = arr.containerId INNER JOIN
                                             dbo.actlink AS al ON arr.referenceObjId = al.actlinkId
                       WHERE     (arc.containerType = 1)
                       UNION ALL
                       SELECT     arc.name, arr.referenceOrder AS seq, f.name AS childName, f.filterId AS childId, coo.ownerObjId AS schemaId, arc.containerId AS ObjectId, 
                                             4 AS ObjectType
                       FROM         dbo.arcontainer AS arc INNER JOIN
                                             dbo.cntnr_ownr_obj AS coo ON arc.containerId = coo.containerId INNER JOIN
                                             dbo.arreference AS arr ON arc.containerId = arr.containerId INNER JOIN
                                             dbo.filter AS f ON arr.referenceObjId = f.filterId
                       WHERE     (arc.containerType = 4)) AS z