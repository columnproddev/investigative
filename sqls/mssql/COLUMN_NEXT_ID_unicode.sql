USE @DBNAME@
IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[COLUMN_NEXT_ID]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE COLUMN_NEXT_ID 
  ( 
     Company                NVARCHAR(255), 
     Form_Name              NVARCHAR(255), 
     Next_ID                NVARCHAR(255), 
     Padding_Char           NVARCHAR(255), 
     Hierarchy_Key_Prefix   NVARCHAR(255), 
     Padding_Int            INT, 
     Prefix_Char            NVARCHAR(255), 
     Prefix_Type            NVARCHAR(255), 
     Record_Key_Field_ID    INT,
     Prefix_Separator       NVARCHAR(255), 
  ) 
