@ECHO OFF
TITLE Create Case Folders

rem+---------------------------------------------------------------------------
rem
rem  File:       CreateCaseFolders.bat
rem
rem  Author:     Brian Meadowcroft
rem              bmeadowcroft@columnit.com
rem
rem  Note:  
rem    This batch creates Case folders
rem+----------------------------------------------------------------------------

:VARIABLES
SET CASE_PATH=%~1
SET YEAR=%~2
SET CASE_ID=%~3
SET GUID=%~4

:CREATE_CASE_FOLDERS
MKDIR "%CASE_PATH%\%YEAR%\%CASE_ID%\%GUID%"
CACLS "%CASE_PATH%\%YEAR%\%CASE_ID%\%GUID%" /T /E /G Users:F

:CREATE_DEFAULT_FOLDERS
MKDIR "%CASE_PATH%\%YEAR%\%CASE_ID%\%GUID%\Correspondence"
MKDIR "%CASE_PATH%\%YEAR%\%CASE_ID%\%GUID%\News"
MKDIR "%CASE_PATH%\%YEAR%\%CASE_ID%\%GUID%\Public Records"
MKDIR "%CASE_PATH%\%YEAR%\%CASE_ID%\%GUID%\Investigative Material"
MKDIR "%CASE_PATH%\%YEAR%\%CASE_ID%\%GUID%\Investigative Material\Investigative Support"
MKDIR "%CASE_PATH%\%YEAR%\%CASE_ID%\%GUID%\Investigative Material\Subpoena"
MKDIR "%CASE_PATH%\%YEAR%\%CASE_ID%\%GUID%\Investigative Material\ROI"